class DiffScore implements Scoring {

	public double score(Job job) {
		return job.w - job.l;
	}
}