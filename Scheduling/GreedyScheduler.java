import java.io.*;
import java.util.ArrayList;
import java.util.Arrays; 
import java.util.Comparator;

class GreedyScheduler {
	
	private Job[] jobs;
	private long completion_time;
	private int n;
	
	public GreedyScheduler(String file, Scoring score_func) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(file));
		n = Integer.parseInt(in.readLine());
		jobs = new Job[n];
		for(int i = 0; i < n; i++) {
			String[] line = in.readLine().split("\\s+");
			jobs[i] = new Job(Integer.parseInt(line[0]), Integer.parseInt(line[1]), score_func);
		}
	}
	
	public void schedule() {
		Arrays.sort(jobs, new Comparator<Job>() {
			public int compare(Job j1, Job j2) {
				if(j1.w < j2.w) {
					return -1;
				} else if(j1.w > j2.w) {
					return 1;
				}	
				return 0;
			}
		});
		Arrays.sort(jobs, new Comparator<Job>() {
			public int compare(Job j1, Job j2) {
				if(j1.get_score() < j2.get_score()) {
					return -1;
				} else if(j1.get_score() > j2.get_score()) {
					return 1;
				}	
				return 0;
			}
		});
		int accum = 0;
		for(int i = n-1; i >= 0; i--) {
			accum += jobs[i].l;
			completion_time += jobs[i].w*accum;
		}
	}
	
	public long get_optimal_completion_time() {
		return completion_time;
	}
	
	public static void main(String[] args) throws IOException {
		Scoring score_func = "diff".equals(args[1]) ? new DiffScore() : new RatioScore();
		GreedyScheduler sch = new GreedyScheduler(args[0], score_func);
		sch.schedule();
		StdOut.println("Completion time found by greedy alg: " + sch.get_optimal_completion_time());
	}
}