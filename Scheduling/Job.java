class Job {
	
	public int w;
	public int l;
	public Scoring score_func;
	
	public Job(int w, int l, Scoring score_func) {
		this.w = w;
		this.l = l;
		this.score_func = score_func;
	}
	
	public double get_score() {
		return score_func.score(this);
	}
}