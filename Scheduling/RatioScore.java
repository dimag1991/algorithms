class RatioScore implements Scoring {

	public double score(Job job) {
		return (job.w+0.0)/job.l;
	}
}