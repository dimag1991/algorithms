import matplotlib.pyplot as plt

ns_half = []
es_half = []

ns_near_half = []
es_near_half = []

lines = [line.strip() for line in open('file_half.txt')]

for l in lines:
    tokens = l.split(' ')
    ns_half.append(int(tokens[0]))
    es_half.append(float(tokens[1]))

lines = [line.strip() for line in open('file_near_half.txt')]

for l in lines:
    tokens = l.split(' ')
    ns_near_half.append(int(tokens[0]))
    es_near_half.append(float(tokens[1]))

plt.plot(ns_half, es_half, 'b',  label='p=0.5') 
plt.plot(ns_near_half, es_near_half, 'r',  label='p=0.49') 

plt.legend(loc='upper left')   