import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

class TwoSAT {
	
	private int n;
	private int m;
	private boolean[] x;
	private int[][] clauses;
	private int[] clause_types;
	private ArrayList<Integer>[] map;
	private int[] unsat_stack;
	private int ptr;
	private boolean[] is_in_stack; //track what already in stack to prevent stack overflow
	
	public TwoSAT(int[][] clauses, int[] clause_types, int n) {
		this.n = n;
		this.m = clause_types.length;
		this.x = new boolean[n];
		this.clauses = clauses;
		this.clause_types = clause_types;
		map = (ArrayList<Integer>[]) new ArrayList[n];
		for(int i = 0; i < n; i++) {
			map[i] = new ArrayList<>();
		}
		for(int i = 0; i < m; i++) {
			map[clauses[0][i]].add(i);
			map[clauses[1][i]].add(i);
		}
	}
	
	public boolean solve() {
		long steps = 2*n;
		initialize();
		for(long k = 0; k < steps; k++) {
			if(!move()) {
				return true;
			}	
		}
		return false;
	}
	
	public void initialize() {
		for(int i = 0; i < n; i++) {
			x[i] = Math.random() < 0.5 ? true : false;
		}
		unsat_stack = new int[m];
		is_in_stack = new boolean[m];
		ptr = 0;
		for(int i = 0; i < m; i++) {
			if(!is_sat(i)) {
				unsat_stack[ptr++] = i;
				is_in_stack[i] = true;
			}
		}
	}
	
	public boolean move() {
		int unsat_clause = -1;
		while(ptr > 0) {
			int curr = unsat_stack[--ptr];
			is_in_stack[curr] = false;
			if(!is_sat(curr)) {
				unsat_clause = curr;
				break;
			}
		}
		if(unsat_clause != -1) {
			int var_to_change = clauses[Math.random() < 0.5 ? 0:1][unsat_clause];
			x[var_to_change] = !x[var_to_change];			
			for(int cl : map[var_to_change]) {
				if(!is_sat(cl) && !is_in_stack[cl]) {
					unsat_stack[ptr++] = cl;
					is_in_stack[cl] = true;
				}
			}
			return true;
		}
		return false;
	}
	
	public boolean is_sat(int id) {
		boolean is_sat = false;
		boolean var1 = x[clauses[0][id]], var2 = x[clauses[1][id]];
		switch(clause_types[id]) {
			case 0: is_sat = !var1 || !var2; break;
			case 1: is_sat = !var1 ||  var2; break;
			case 2: is_sat =  var1 || !var2; break;
			case 3: is_sat =  var1 ||  var2; break;
		}
		return is_sat;
	}
	
	public static double log_2(double a) {
		return Math.log(a)/Math.log(2);
	}
	
	public static void main(String[] args) throws IOException {
		Stopwatch sw = new Stopwatch();
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		int n = Integer.parseInt(in.readLine()), m = n;
		int[][] clauses = new int[2][m];
		int[] clause_types = new int[m];
		for(int i = 0; i < m; i++) {
			String[] line = in.readLine().split("\\s+");
			int x1 = Integer.parseInt(line[0]), x2 = Integer.parseInt(line[1]);
			clauses[0][i] = (x1 > 0) ? (x1-1):(-x1-1);
			clauses[1][i] = (x2 > 0) ? (x2-1):(-x2-1);
			if(x1 < 0 && x2 < 0) {
				clause_types[i] = 0;
			} else if(x1 < 0 && x2 > 0) {
				clause_types[i] = 1;
			} else if(x1 > 0 && x2 < 0) {
				clause_types[i] = 2;
			} else {
				clause_types[i] = 3;
			}
		}		
		TwoSAT sat = new TwoSAT(clauses, clause_types, n);
		if(sat.solve()) {
			StdOut.println("\n\nThis 2SAT instance is SAT");
		} else {
			StdOut.println("\n\nThis 2SAT instance is unSAT");
		}
		StdOut.println("\n\nTiming: " + sw.elapsedTime()); 
	}
}