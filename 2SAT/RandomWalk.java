class RandomWalk {
	
	public static void random_walk(int n1, int n2, int step, int T, double p) {
		for(int n = n1; n <= n2; n += step) {
			double expected = 0;
			for(int t = 0; t < T; t++) {
				int steps = 0;
				int pos = StdRandom.uniform(0, n+1);
				while(true) {
					if(pos == n) {
						expected += steps;
						break;
					}
					if(pos == 0) {
						pos++;
					} else {
						pos = Math.random() < p ? (pos-1):(pos+1);
					}
					steps++;
				}
			}
			StdOut.println(n + " " + expected/T);
		}
	}
	
	public static void main(String[] args) {
		int n2 = Integer.parseInt(args[0]);
		double p = Double.parseDouble(args[1]);
		int T = Integer.parseInt(args[2]);
		random_walk(10, 90, 10, T, p);
		random_walk(100, n2, 500, T, p);
	}
}