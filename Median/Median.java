import java.io.*;

class Median {

	private int median;
	private MinPQ<Integer> Hhigh;
	private MaxPQ<Integer> Hlow;
	
	public Median() {
		Hhigh = new MinPQ<>();
		Hlow = new MaxPQ<>();
	}
	
	public void add_number(int next) {
		int left = Hlow.size(), right = Hhigh.size();
		int lo = Hlow.isEmpty() ? Integer.MIN_VALUE : Hlow.max();
		int hi = Hhigh.isEmpty() ? Integer.MAX_VALUE : Hhigh.min();
		if(next > lo && next < hi) {
			if(left > right) {
				Hhigh.insert(next);
			} else {
				Hlow.insert(next);
			}
		} else if(next < lo) {
			if(left > right) {
				Hhigh.insert(lo);
				Hlow.delMax();
				Hlow.insert(next);
			} else {
				Hlow.insert(next);
			}
		} else if(next > hi) {
			if(left < right) {
				Hlow.insert(hi);
				Hhigh.delMin();
				Hhigh.insert(next);
			} else {
				Hhigh.insert(next);
			}
		}
		left = Hlow.size();
		right = Hhigh.size();
		int k = left + right;
		int mid = (k+1)/2;
		median = (left == mid) ? Hlow.max() : Hhigh.min();
	}
	
	public int get_median() {
		return median;
	}
	
	public static void main(String[] args) throws IOException {
		long median_sum = 0; 
		int size = Integer.parseInt(args[1]);
		Median m = new Median();
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		for(int i = 0; i < size; i++) {
			int next = Integer.parseInt(in.readLine());
			m.add_number(next);
			median_sum += m.get_median();
		}
		in.close();
		StdOut.println("(Sum of medians) mod 10000 = " + median_sum % 10000);
	}
}