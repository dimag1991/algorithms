import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

class SCC {

	private int n;
	private boolean[] marked;
	private int[] leaders;
	private int[] f;
	private int t = 0;
	private int s = -1;
	
	public SCC(ArrayList<Integer>[] G) {
		n = G.length;
		ArrayList<Integer>[] G_rev = (ArrayList<Integer>[]) new ArrayList[n];
		for(int i = 0; i < n; i++) {
			G_rev[i] = new ArrayList<>();
		}
		for(int i = 0; i < n; i++) {
			for(int j : G[i]) {
				G_rev[j].add(i);
			}
		}
		marked = new boolean[n];
		leaders = new int[n];
		f = new int[n];
		for(int i = n-1; i >= 0; i--) {
			if(!marked[i]) {
				s = i;
				dfs(G_rev, i);
			}
		}
		marked = new boolean[n];
		int[] order = new int[n];
		for(int i = 0; i < n; i++) {
			order[f[i]] = i;
		}
		for(int i = n-1; i >= 0; i--) {
			if(!marked[order[i]]) {
				s = i;
				dfs(G, order[i]);
			}
		}
	}
	
	public void dfs(ArrayList<Integer>[] G, int i) {
		marked[i] = true;
		leaders[i] = s;
		for(int v : G[i]) {
			if(!marked[v]) {
				dfs(G, v);
			}
		}
		f[i] = t;
		t++;
	}
	
	public void print_top5() {
		HashMap<Integer, Integer> scc_sizes = new HashMap<>();
		for(int i = 0; i < n; i++) {
			if(scc_sizes.containsKey(leaders[i])) {
				scc_sizes.put(leaders[i], 1 + scc_sizes.get(leaders[i]));
			} else {
				scc_sizes.put(leaders[i], 1);
			}
		}
		MinPQ top = new MinPQ();
		for(int scc : scc_sizes.keySet()) {
			top.insert(scc_sizes.get(scc));
			if(top.size() > 5) {
				top.delMin();
			}
		}
		StdOut.println();
		for(int i = 0; i < 5; i++) {
			StdOut.println("SCC" + i + ": " + top.delMin());
		}
	}
	
	public static void main(String[] args) throws IOException {
		Stopwatch sw = new Stopwatch();
		int n = Integer.parseInt(args[1]);
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		String line = null;
		ArrayList<Integer>[] G = (ArrayList<Integer>[]) new ArrayList[n];
		for(int i = 0; i < n; i++) {
			G[i] = new ArrayList<>();
		}
		while((line = in.readLine()) != null) {
			String[] v1_v2 = line.split("\\s+");
			int v1 = Integer.parseInt(v1_v2[0]) - 1, v2 = Integer.parseInt(v1_v2[1]) - 1;
			G[v1].add(v2);
		}
		in.close();
		SCC scc = new SCC(G);
		scc.print_top5();
		StdOut.println("\n\nTiming: " + sw.elapsedTime());
	}
}