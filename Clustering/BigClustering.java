import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;

class BigClustering {
	
	//assumes that only 24 LSBs may be non-zero
	public static boolean is_hamming_greater_than_2(int xor_mask) {
		int dist = 0 ;
		for(int i = 0; i < 24; i++) {
			if((xor_mask&1) == 1) {
				dist++;
				if(dist > 2) {
					return true;
				}
			}
			xor_mask >>= 1;
		}
		return false;
	}
	
	public static int run_clustering(HashMap<String, ArrayList<Integer>> index, int[] node_ints, int n) {
		int count = 0, tot = index.size();
		FlatUF uf = new FlatUF(n);		
		for(String s : index.keySet()) {
			count++;
			StdOut.println(count + "/" + tot + ": '" + s + "'");
			Integer[] ids = index.get(s).toArray(new Integer[index.get(s).size()]);
			int len = ids.length;
			for(int i = 0; i < len; i++) {
				for(int j = i + 1; j < len; j++) {
					if(!is_hamming_greater_than_2(node_ints[ids[i]] ^ node_ints[ids[j]])) {
						uf.union(ids[i], ids[j]);
					} 
				}
			}
		}
		return uf.cc_num();
	}
	
	public static int bits_to_int(String[] bits) {
		int n = 0;
		for(int i = 0; i < bits.length; i++) {
			n = 2*n + (bits[i].charAt(0)-48); 
		}
		return n;
	}
	
	public static void main(String[] args) throws IOException {		
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		String[] line = in.readLine().split("\\s+");
		int n = Integer.parseInt(line[0]);
		int bit_num = Integer.parseInt(line[1]);
		HashSet<String> nodes = new HashSet<>();
		for(int i = 0; i < n; i++) {
			nodes.add(in.readLine());
		}
		in.close();
		int distinct_num = nodes.size();
		String[] node_bits = new String[distinct_num];
		int[] node_ints = new int[distinct_num];
		int j = 0;
		for(String s : nodes) {
			node_bits[j] = s;
			node_ints[j] = bits_to_int(s.split("\\s+"));
			j++;
		}
		nodes = null; //free memory that not needed any more
		HashMap<String, ArrayList<Integer>> index = new HashMap<>();
		for(int i = 0; i < distinct_num; i++) {			
			String left = "l" + node_bits[i].substring(0, 15);
			String mid = "m" + node_bits[i].substring(16, 31);
			String right = "r" + node_bits[i].substring(32, 47); 
			if(index.containsKey(left)) {
				index.get(left).add(i);
			} else {
				index.put(left, new ArrayList<Integer>());
				index.get(left).add(i);
			}
			if(index.containsKey(mid)) {
				index.get(mid).add(i);
			} else {
				index.put(mid, new ArrayList<Integer>());
				index.get(mid).add(i);
			}
			if(index.containsKey(right)) {
				index.get(right).add(i);
			} else {
				index.put(right, new ArrayList<Integer>());
				index.get(right).add(i);
			}
		}
		node_bits = null; //free memory that not needed any more
		Stopwatch sw = new Stopwatch();
		StdOut.println("\nMax number of clusters = " + run_clustering(index, node_ints, distinct_num));
		StdOut.println("\n\nTiming: " + sw.elapsedTime());
	}
}