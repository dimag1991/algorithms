import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Arrays;

class MaxSpacingkClustering {
	
	private long max_spacing;
	private int[] cluster_ids;
	private int k, n;
	
	public MaxSpacingkClustering(int k, int n) {
		this.k = k;
		this.n = n;
		cluster_ids = new int[n];
	}
	
	public void cluster(Arc[] edges) {
		int m = edges.length;
		Arrays.sort(edges);
		FlatUF uf = new FlatUF(n);
		Stopwatch sw = new Stopwatch();
		for(int i = 0; i < m; i++) {
			int v1 = edges[i].v1, v2 = edges[i].v2;
			if(uf.is_connected(v1, v2)) {
				continue;
			}
			if(uf.cc_num() == k) {
				max_spacing = edges[i].cost;
				cluster_ids = uf.get_leaders();
				break;
			}
			uf.union(v1, v2);
		}
		StdOut.println("\nTiming: " + sw.elapsedTime() + "\n");
	}
	
	public long get_max_spacing() {
		return max_spacing;
	}
	
	public static void main(String[] args) throws IOException {
		int k = Integer.parseInt(args[0]);
		BufferedReader in = new BufferedReader(new FileReader(args[1]));
		int n = Integer.parseInt(in.readLine());
		int m = n*(n-1)/2;
		Arc[] edges = new Arc[m];
		for(int i = 0; i < m; i++) {
			String[] line = in.readLine().split("\\s+");
			edges[i] = new Arc(Integer.parseInt(line[0])-1, Integer.parseInt(line[1])-1, Integer.parseInt(line[2]));
		}
		in.close();
		MaxSpacingkClustering mskcl = new MaxSpacingkClustering(k, n);
		mskcl.cluster(edges);
		StdOut.println("Max spacing = " + mskcl.get_max_spacing());
	}
}