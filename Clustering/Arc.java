class Arc implements Comparable<Arc> {
	
	public int v1, v2;
	public int cost;
	
	public Arc(int v1, int v2, int cost) {
		this.v1 = v1;
		this.v2 = v2;
		this.cost = cost;
	}
	
	public int compareTo(Arc that) {
		if(this.cost < that.cost) {
			return -1;
		} else if(this.cost > that.cost) {
			return 1;
		}
		return 0;
	}
}