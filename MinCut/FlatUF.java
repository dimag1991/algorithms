class FlatUF {

	private int N;
	private int[] leaders;
	private int[] size;
	private int cc_num;

	public FlatUF(int N) {
		this.N = N;
		cc_num = N;
		leaders = new int[N];
		size = new int[N];
		for(int i = 0; i < N; i++) {
			leaders[i] = i;
			size[i] = 1;
		}
	}
	
	public int[] get_leaders() {
		return leaders;
	}
	
	public int cc_num() {
		return cc_num;
	}
	
	public boolean is_connected(int a, int b) {
		return find(a) == find(b);
	}
	
	public int find(int a) {
		return leaders[a];
	}
	
	public void union(int a, int b) {
		if(is_connected(a, b)) {
			return;
		}
		cc_num--;
		int component1 = find(a);
		int component2 = find(b);
		int size1 = size[component1];
		int size2 = size[component2];
		int union_component_lead = size1 > size2 ? component1 : component2;
		int old_lead = (union_component_lead == component1) ? component2 : component1;
		size[union_component_lead] += size[old_lead];
		for(int i = 0; i < N; i++) {
			if(leaders[i] == old_lead) {
				leaders[i] = union_component_lead;
			}
		}
	}
}