import java.io.*;
import java.util.HashSet;
import java.util.ArrayList;

class MinCut {
	
	public static int cut(ArrayList<Arc> edges, int n) {
		ArrayList<Arc> arcs = new ArrayList<>();
		for(Arc a : edges) {
			arcs.add(a);
		}
		FlatUF uf = new FlatUF(n);
		while(uf.cc_num() != 2) {
			int victim = StdRandom.uniform(0, arcs.size());
			Arc e = arcs.remove(victim);
			uf.union(e.v1, e.v2);
		}
		int cut_size = 0;
		for(Arc e : arcs) {
			if(!uf.is_connected(e.v1, e.v2)) {
				cut_size++;
			}
		}
		return cut_size;
	}
	
	public static int min_cut(ArrayList<Arc> edges, int n, int T) {
		int min = Integer.MAX_VALUE;
		for(int i = 0; i < T; i++) {
			int curr = cut(edges, n);
			if(min > curr) {
				min = curr;
			}
		}
		return min;
	}
	
	public static void main(String[] args) throws IOException {
		Stopwatch sw = new Stopwatch();
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		int n = Integer.parseInt(args[1]);
		int T = Integer.parseInt(args[2]);
		ArrayList<Arc> edges = new ArrayList<Arc>();
		for(int i = 0; i < n; i++) {
			String[] line = in.readLine().split("\\s+");
			for(int k = 1; k < line.length; k++) {
				int v = Integer.parseInt(line[k])-1;
				if(i < v) {
					edges.add(new Arc(i, v));
				}
			}
		}
		in.close();
		StdOut.println("\nMin cut size = " + min_cut(edges, n, T));
		StdOut.println("\n\nTiming: " + sw.elapsedTime());
	}
}