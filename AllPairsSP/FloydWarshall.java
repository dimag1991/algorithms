import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

class FloydWarshall {
	
	private boolean has_neg_cycles = false;
	private double[][][] A;
	private int n;
	
	public FloydWarshall(HashMap<Integer, Double>[] graph) {
		n = graph.length-1;
		A = new double[n+1][][];
		A[0] = new double[n+1][n+1];
		for(int i = 1; i <= n; i++) {
			for(int j = 1; j <= n; j++) {
				if(graph[i].containsKey(j)) {
					A[0][i][j] = graph[i].get(j);
				} else if(i != j) {
					A[0][i][j] = Double.POSITIVE_INFINITY;
				}
			}
		}
		for(int k = 1; k <= n; k++) {
			A[k] = new double[n+1][n+1];
			for(int i = 1; i <= n; i++) {
				for(int j = 1; j <= n; j++) {			
					double tmp = A[k-1][i][k] + A[k-1][k][j];
					A[k][i][j] = Math.min(A[k-1][i][j], tmp);					
				}
			}
			A[k-1] = null;
		}
		for(int i = 1; i <= n; i++) {
			if(A[n][i][i] < 0) {
				StdOut.println("\nGraph has negative cycle: " + A[n][i][i] + "\n");
				has_neg_cycles = true;
				break;
			}
		}
	}
	
	public Double sshortest_path() {
		if(!has_neg_cycles) {
			double ssp = Double.POSITIVE_INFINITY;
			for(int i = 1; i <= n; i++) {
				for(int j = 1; j <= n; j++) {
					if(A[n][i][j] < ssp) {
						ssp = A[n][i][j];
					}
				}
			}
			return ssp;
		}
		return null;
	}
	
	public static void main(String[] args) throws IOException {
		Stopwatch sw = new Stopwatch();
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		String[] line = in.readLine().split("\\s+");
		int n = Integer.parseInt(line[0]), m = Integer.parseInt(line[1]);
		HashMap<Integer, Double>[] graph = (HashMap<Integer, Double>[]) new HashMap[n+1]; 
		for(int i = 0; i <= n; i++) {
			graph[i] = new HashMap<Integer, Double>();
		}
		for(int i = 0; i < m; i++) {
			line = in.readLine().split("\\s+");
			graph[Integer.parseInt(line[0])].put(Integer.parseInt(line[1]), Double.parseDouble(line[2]));
		}
		in.close();
		FloydWarshall fw = new FloydWarshall(graph);
		StdOut.println("\nShortest shortest path: " + fw.sshortest_path());
		StdOut.println("\n\nTiming: " + sw.elapsedTime());
	}
}