class InversionsCounterN2 {
	public static void main(String[] args) {
		Stopwatch sw = new Stopwatch();
		int N = Integer.parseInt(args[0]);
		int[] a = new int[N];
		for(int i = 0; i < N; i++) {
			a[i] = Integer.parseInt(StdIn.readLine());
		}
		long invNum = 0;
		for(int i = 0; i < N; i++) {
			for(int j = i + 1; j < N; j++) {
				if(a[i] > a[j]) invNum++;
			}
		}
		StdOut.println(invNum + "\nTime: " + sw.elapsedTime());
	}
}