class InversionsCounterNlogN {

	public static long countSplitInversions(int[] a, int[] aux, int lo, int mid, int hi) {
		for(int k = lo; k <= hi; k++) {
			aux[k] = a[k];
		}
		int i = lo;
		int j = mid + 1;
		long splitInv = 0;
		for(int k = lo; k <= hi; k++) {
			if(i > mid) {
				a[k] = aux[j++];
			} else if(j > hi) {
				a[k] = aux[i++];
			} else if(aux[j] < aux[i]) {
				a[k] = aux[j++];	
				splitInv += mid - i + 1;
			} else {
				a[k] = aux[i++];
			}
		}
		return splitInv;
	}
	
	public static long inversionsCount(int[] a, int[] aux, int lo, int hi) {
		if(hi <= lo) return 0;
		int mid = lo + (hi - lo)/2;
		long x = inversionsCount(a, aux, lo, mid);
		long y = inversionsCount(a, aux, mid + 1, hi);
		return x + y + countSplitInversions(a, aux, lo, mid, hi);
	}

	public static long getInversionsNumber(int[] a) {
		int[] aux = new int[a.length];
		return inversionsCount(a, aux, 0, a.length - 1);
	}

	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		int[] a = new int[N];
		for(int i = 0; i < N; i++) {
			a[i] = StdIn.readInt();
		}
		long invNum = 0;
		Stopwatch sw = new Stopwatch();
		invNum = getInversionsNumber(a);
		StdOut.println("Time: " + sw.elapsedTime() + "\n" + "InversionsNum: " + invNum);
	}	
}