#include <iostream>
#include <time.h>
using namespace std;

int main() {
	double t = clock();
	int N = 100000;
	int* a = new int[N];
	for(int i = 0; i < N; i++) {
		cin >> a[i]; 
	}
	unsigned int invNum = 0;
	for(int i = 0; i < N; i++) {
		for(int j = i + 1; j < N; j++) {
			if(a[i] > a[j]) invNum++;
		}
	}
	cout << invNum << endl;
	cout << endl << (clock() - t)/CLOCKS_PER_SEC << endl;
	return 0;
}