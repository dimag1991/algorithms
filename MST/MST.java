import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;

class MST {
	
	private ArrayList<Arc> mst = new ArrayList<>();
	private double cost;
	
	public MST(ArrayList<Arc>[] graph, int n, int m) {
		HashSet<Integer> spanned = new HashSet<>();
		spanned.add(0);
		MinPQ<Arc> crossing = new MinPQ<>();
		for(Arc e : graph[0]) {
			crossing.insert(e);
		}
		while(spanned.size() < n) {
			Arc e = crossing.delMin();
			int new_v = spanned.contains(e.v1) ? e.v2 : e.v1;
			if(spanned.contains(new_v)) {
				continue;
			}
			mst.add(e);
			spanned.add(new_v);
			for(Arc edge : graph[new_v]) {
				int other_v = (new_v == edge.v1) ? edge.v2 : edge.v1;
				if(!spanned.contains(other_v)) {
					crossing.insert(edge);
				}
			}
		}
		for(Arc e : mst) {
			cost += e.cost;
		} 
	}
	
	public double cost() {
		return cost;
	}	
	
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		String[] line = in.readLine().split("\\s+");
		int n = Integer.parseInt(line[0]);
		int m = Integer.parseInt(line[1]);
		ArrayList<Arc>[] graph = (ArrayList<Arc>[]) new ArrayList[n];
		for(int i = 0; i < n; i++) {
			graph[i] = new ArrayList<>();
		}
		for(int i = 0; i < m; i++) {
			line = in.readLine().split("\\s+");
			int v1 = Integer.parseInt(line[0])-1, v2 = Integer.parseInt(line[1])-1;
			Arc e = new Arc(v1, v2, Double.parseDouble(line[2]));
			graph[v1].add(e);
			graph[v2].add(e);
		}
		MST mst = new MST(graph, n, m);
		StdOut.println("\nMST cost = " + mst.cost());
		in.close(); 
	}
}