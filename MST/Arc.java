class Arc implements Comparable<Arc> {
	
	public int v1, v2;
	public double cost;
	
	public Arc(int v1, int v2, double cost) {
		this.v1 = v1;
		this.v2 = v2;
		this.cost = cost;
	}
	
	public int compareTo(Arc that) {
		if(this.cost < that.cost) {
			return -1;
		} else if(this.cost > that.cost) {
			return 1;
		}
		return 0;
	}
}