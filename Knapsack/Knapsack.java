import java.io.*;
import java.util.HashSet;
import java.util.HashMap;

class Knapsack {
	
	private int W;
	private long value;
	private int n;
	
	public Knapsack(int W) {
		this.W = W;
	}
	
	public void dp_fill(int[] v, int[] w) {
		StdOut.println("\nDP algorithm ... \n");
		n = v.length;
		int[][] A = new int[n+1][];
		A[0] = new int[W+1];
		for(int i = 0; i <= W; i++) {
			A[0][i] = 0;
		}
		for(int i = 1; i <= n; i++) {
			A[i] = new int[W+1];
			for(int x = 0; x <= W; x++) {
				if(x < w[i-1]) {
					A[i][x] = A[i-1][x];
				} else {
					A[i][x] = Math.max(A[i-1][x], A[i-1][x-w[i-1]] + v[i-1]);
				}
			}
			//in the next iteration of the knapsack problem will be relevant only the 'row' computed in this iteration
			//previous iteration results can be discarded			
			A[i-1] = null; 
		}
		value = A[n][W];
	}
	
	public void memoization_fill(int[] v, int[] w) {
		StdOut.println("\nMemoization algorithm ... \n");
		n = v.length;
		HashMap<Integer, Long>[] cache = (HashMap<Integer, Long>[]) new HashMap[W+1];
		for(int i = 0; i <= W; i++) {
			cache[i] = new HashMap<>();
		} 
		value = memo_fill(v, w, n, W, cache);
		int size = 0;
		for(int i = 0; i <= W; i++) {
			size += cache[i].size();
		}
		StdOut.println("Cache size = " + size);
	}
	
	public long memo_fill(int[] v, int[] w, int i, int cap, HashMap<Integer, Long>[] cache) {
		if(i == 0) {
			return 0;
		}
		long subproblem1, subproblem2;
		if(cache[cap].containsKey(i-1)) {
			subproblem1 = cache[cap].get(i-1);
		} else {
			subproblem1 = memo_fill(v, w, i-1, cap, cache);
			cache[cap].put(i-1, subproblem1);
		}
		if(cap < w[i-1]) {
			return subproblem1;
		}
		if(cache[cap - w[i-1]].containsKey(i-1)) {
			subproblem2 = cache[cap - w[i-1]].get(i-1);
		} else {
			subproblem2 = memo_fill(v, w, i-1, cap - w[i-1], cache);
			cache[cap - w[i-1]].put(i-1, subproblem2);
		}		
		return Math.max(subproblem1, subproblem2 + v[i-1]);
	}
	
	public long get_value() {
		return value;
	}
	
	public static void main(String[] args) throws IOException {
		Stopwatch sw = new Stopwatch();
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		String[] line = in.readLine().split("\\s+");
		int W = Integer.parseInt(line[0]); 
		int n = Integer.parseInt(line[1]);
		int[] w = new int[n];
		int[] v = new int[n];
		for(int i = 0; i < n; i++) {
			line = in.readLine().split("\\s+");
			v[i] = Integer.parseInt(line[0]); 
			w[i] = Integer.parseInt(line[1]);
		}
		Knapsack k = new Knapsack(W);
		k.dp_fill(v, w);
		//k.memoization_fill(v, w);
		StdOut.println("Optimal value = " + k.get_value());
		StdOut.println("\nTiming: " + sw.elapsedTime());
		in.close();
	}
}