import java.io.*;
import java.util.HashSet;
import java.util.HashMap;
import java.util.ArrayList;

class TwoSum {
	
	private int target_number;
	
	public TwoSum(long[] nums, long Tmin, long Tmax) {
		long w = (Tmax-Tmin);
		HashSet<Long> sums = new HashSet<>();
		HashMap<Long, HashSet<Long>> map = new HashMap<>();
		for(int i = 0; i < nums.length; i++) {
			if(map.containsKey(nums[i]/w)) {
				map.get(nums[i]/w).add(nums[i]);
			} else {
				map.put(nums[i]/w, new HashSet<Long>());
				map.get(nums[i]/w).add(nums[i]);
			}
		}
		for(int i = 0; i < nums.length; i++) {
			HashSet<Long> s = map.get((Tmin - nums[i])/w);
			if(s != null) {
				for(long l : s) {
					if(l != nums[i] && l+nums[i] >= Tmin && l+nums[i] <= Tmax) {
						sums.add(l+nums[i]);
					}
				}
			}
			s = map.get((Tmin - nums[i])/w + 1);
			if(s != null) {
				for(long l : s) {
					if(l != nums[i] && l+nums[i] >= Tmin && l+nums[i] <= Tmax) {
						sums.add(l+nums[i]);
					}
				}
			}
		}
		target_number = sums.size();
	}
	
	public int get_target_number() {
		return target_number;
	}
	
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		long[] nums = new long[1000000];
		for(int i = 0; i < nums.length; i++) {
			nums[i] = Long.parseLong(in.readLine());
		}
		in.close();
		long Tmin = -10000, Tmax = 10000;
		Stopwatch sw = new Stopwatch();
		TwoSum ts = new TwoSum(nums, Tmin, Tmax);
		StdOut.println("\nTargets number = " + ts.get_target_number());
		StdOut.println("\n\nTiming results: " + sw.elapsedTime());
	}
}