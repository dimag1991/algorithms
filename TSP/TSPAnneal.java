import java.io.*;
import java.util.ArrayList;

class TSPAnneal {
	
	private int N;
	private double opt_tour_len;
	private double[] x;
	private double[] y;
	private int[] opt_tour;
	
	public TSPAnneal(double[] x, double[] y) {
		this.N = x.length;
		this.x = x;
		this.y = y;
		opt_tour = new int[N];
	}
	
	public void solve() {
		int[] tour = new int[N];
		double tour_len = 0;
		for(int i = 0; i < N; i++) {
			tour[i] = (i+1)%N;
			opt_tour[i] = tour[i];
			tour_len += dist(i, (i+1)%N);
		}
		opt_tour_len = tour_len;
		double T = 1000;
		while(T > 0.000000001) {
			for(int t = 0; t < 30000; t++) {
				int e1 = StdRandom.uniform(0, N);
				int e2 = StdRandom.uniform(0, N);
				if(e1 == e2 || tour[e1] == e2 || tour[e2] == e1) {
					continue;
				}
				double new_len = tour_len - dist(e1, tour[e1]) - dist(e2, tour[e2]) + dist(e1, e2) + dist(tour[e1], tour[e2]);
				if(new_len < tour_len || Math.random() < Math.exp(-(new_len - tour_len)/T)) {
					tour_len = new_len;
					Stack<Integer> stack = new Stack<>();
					for(int i = tour[e1]; i != tour[e2]; i = tour[i]) {
						stack.push(i);
					}
					tour[tour[e1]] = tour[e2];
					tour[e1] = e2;
					int curr = stack.pop();
					while(!stack.isEmpty()) {
						int next = stack.pop();
						tour[curr] = next;
						curr = next;
					}
					if(opt_tour_len > tour_len) {
						opt_tour_len = tour_len;
					}
				}
			}	
			T *= 0.99;
		}	
	}
	
	public double get_opt_tour_len() {
		return opt_tour_len;
	}
	
	public float dist(int i, int j) {
		return (float) Math.sqrt((x[i]-x[j])*(x[i]-x[j]) + (y[i] - y[j])*(y[i] - y[j]));
	}
	
	public static void main(String[] args) throws IOException {
		Stopwatch sw = new Stopwatch();
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		int N = Integer.parseInt(in.readLine());
		double[] x = new double[N];
	    double[] y = new double[N];		
		for(int i = 0; i < N; i++) {
			String[] line = in.readLine().split("\\s+");
			//x[i] = Double.parseDouble(line[0]);
			//y[i] = Double.parseDouble(line[1]);	
			x[i] = Double.parseDouble(line[1]);
			y[i] = Double.parseDouble(line[2]);				
		}
		in.close();
		TSPAnneal tsp = new TSPAnneal(x, y);
		tsp.solve();
		StdOut.println("\n\nOptimal tour length = " + tsp.get_opt_tour_len());
		StdOut.println("\n\nTiming: " + sw.elapsedTime());	
	}
}