import java.io.*;

class TSP {
	
	public static int memory = 0;
	
	private int N;
	private float opt_tour_len;
	private double[] x;
	private double[] y;
	
	public TSP(double[] x, double[] y) {
		this.N = x.length;
		this.x = x;
		this.y = y;
	}
	
	public void solve() {
		float[][] dist = new float[N][N];
		for(int i = 0; i < N; i++) {
			for(int j = 0; j < N; j++) {
				dist[i][j] = dist(i, j);
			}
		} 		
		int C = C(N-1, (N-1)/2);
		float[] curr = new float[N*C];
		float[] prev = new float[N*C];		
		int size = 1 << N;		
		int[] set_ids = new int[size/2];	
		int[] ptrs = new int[N];		
		//implicitly assume the LSB to be 1
		for(int i = 0; i < size/2; i++) {
			int ones_num = count_bit_ones(i);
			set_ids[i] = ptrs[ones_num];				
			ptrs[ones_num]++;
		}	
		memory += (2*N*C*4 + 4*size/2);
		float[] tmp;		
		for(int m = 2; m <= N; m++) {
			//StdOut.print(m + " ");
			for(int s = ((-1) << (m-1)) ^ (-1), i = 0; s < size/2; s = next_combination(s), i++) { //applying Gaspar's hack
				int S = (s << 1) + 1; //ensure that first vertex '1' is included to the set S
				for(int j = 1; j < N; j++) {
					if(((S >> j) & 1) == 1) {					
						int prev_set = (S ^ (1 << j));
						int prev_set_id_N = N*set_ids[prev_set >> 1];
						float min = (prev_set != 1) ? Float.POSITIVE_INFINITY : dist[j][0]; //explicit iteration for k = 0 (loop unrolling)
						for(int k = 1; k < N; k++) {
							if((k != j) && (((S >> k) & 1) == 1)) {
								float curr_dist = dist[j][k] + prev[prev_set_id_N + k];
								min = (curr_dist < min) ? curr_dist : min;
							}
						} 
						curr[j + i*N] = min;
					}
				}
			}
			tmp = prev;
			prev = curr;
			curr = tmp;
		}
		opt_tour_len = Float.POSITIVE_INFINITY;
		for(int j = 1; j < N; j++) {
			float curr_dist = dist[j][0] + prev[j + N*set_ids[size/2 - 1]];
			if(curr_dist < opt_tour_len) {
				opt_tour_len = curr_dist;
			}
		}   
	}
	
	public float dist(int i, int j) {
		return (float) Math.sqrt((x[i]-x[j])*(x[i]-x[j]) + (y[i] - y[j])*(y[i] - y[j]));
	}
	
	public float get_opt_tour_len() {
		return opt_tour_len;
	}
	
	public static int C(int n, int k) {
		double answer = 1.0;
		for (int i = 1; i <= k; i++) {
			answer *= (n + 1 - i);
			answer /= i;         
		}
		return (int) answer;
	}	

	public static int count_bit_ones(int a) {
		int count = 0;
		for(int i = 0; i < 32; i++) {
			if(((a >> i) & 1)== 1) {
				count++;
			}
		}
		return count;
	}
	
	public static int next_combination(int x) {
		int u = x & -x; 
		int v = u + x; 
		x = v + (((v^x)/u) >> 2); 
		return x;
	}
	
	public static void main(String[] args) throws IOException {
		Stopwatch sw = new Stopwatch();
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		int N = Integer.parseInt(in.readLine());
		double[] x = new double[N];
	    double[] y = new double[N];		
		for(int i = 0; i < N; i++) {
			String[] line = in.readLine().split("\\s+");
			x[i] = Double.parseDouble(line[0]);
			y[i] = Double.parseDouble(line[1]);		
		}
		in.close();
		TSP tsp = new TSP(x, y);
		tsp.solve();
		StdOut.println("\n\nOptimal tour length = " + tsp.get_opt_tour_len());
		StdOut.println("\n\nTiming: " + sw.elapsedTime());
		StdOut.println("\n\nMemory usage: " + memory/1000000 + " Mb") ;		
	}
}