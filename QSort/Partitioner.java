abstract class Partitioner {
	
	public void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
	
	public int partition(int[] a, int l, int r) {
		int p = a[l];
		int i = l+1;
		for(int j = l+1; j < r; j++) {
			if(a[j] < p) {
				swap(a, i, j);
				i++;
			}
		}
		swap(a, l, i-1);
		return i-1;
	}
	
	abstract public int apply(int[] a, int l, int r);
}