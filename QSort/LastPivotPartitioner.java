class LastPivotPartitioner extends Partitioner {
	
	public int apply(int[] a, int l, int r) {
		swap(a, r-1, l);
		return partition(a, l, r);
	}
}