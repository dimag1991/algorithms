class FirstPivotPartitioner extends Partitioner {
	
	public int apply(int[] a, int l, int r) {
		return partition(a, l, r);
	}
}