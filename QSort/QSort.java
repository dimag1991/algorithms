import java.io.*;

class QSort {

	private int comparision_count;
	private Partitioner partitioner;
	
	public QSort(Partitioner partitioner) {
		this.partitioner = partitioner;
	}
	
	public void sort(int[] a) {
		comparision_count = 0;
		sort(a, 0, a.length);
	}	
	
	public void sort(int[] a, int l, int r) {
		int size = r-l;
		if(size <= 1) {
			return;
		}
		comparision_count += (size-1);
		int mid = partitioner.apply(a, l, r);
		sort(a, l, mid);
		sort(a, mid+1, r);
	}
	
	public int get_comparision_count() {
		return comparision_count;
	}
	
	public static void main(String[] args) throws IOException {
		int n = Integer.parseInt(args[1]);
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		int[] a = new int[n];
		for(int i = 0; i < n; i++) {
			a[i] = Integer.parseInt(in.readLine());
		}
		in.close();
		QSort qs = new QSort(new FirstPivotPartitioner());
		try {
			if("median".equals(args[2])) {
				qs = new QSort(new MedianOfThreePivotPartitioner());
			} 
			if("last".equals(args[2])) {
				qs = new QSort(new LastPivotPartitioner());
			}
		} catch (Exception e) {
			StdOut.println("\nUses a default partitioner!\n");
		}
		qs.sort(a);
		StdOut.println("Comparisions count = " + qs.get_comparision_count()); 
	}
}