class MedianOfThreePivotPartitioner extends Partitioner {
	
	public int apply(int[] arr, int l, int r) {
		int middle = l+(r-l-1)/2;
		int median = middle;
		int a = arr[l], b = arr[middle], c = arr[r-1];
		if((a > b && a < c) || (a < b && a > c)) {
			median = l;
		} else {
			if((c > a && c < b) || (c < a && c > b)) {
				median = r-1;
			}
		}
		swap(arr, median, l);
		return partition(arr, l, r);
	}
}