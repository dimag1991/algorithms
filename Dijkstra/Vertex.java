class Vertex implements Comparable<Vertex> {
	
	public int id, cost;
	
	public Vertex(int id, int cost) {
		this.id = id;
		this.cost = cost;
	}
	
	public int compareTo(Vertex that) {
		if(this.cost < that.cost) {
			return -1;
		} else if(this.cost > that.cost) {
			return 1;
		}
		return 0;
	}
}