import java.io.*;
import java.util.HashMap;

class Dijkstra {

	private int n;
	private int[] path_lengths;
	private int source;

	public Dijkstra(HashMap<Integer, Integer>[] graph, int source) {
		this.source = source;
		n = graph.length;
		path_lengths = new int[n];
		for(int i = 0; i < n; i++) {
			path_lengths[i] = 1000000;
		}		
		MinPQ<Vertex> frontier = new MinPQ<>();
		frontier.insert(new Vertex(source, 0));		
		int max = 1;
		while(!frontier.isEmpty()) {
			if(frontier.size() > max) {
				max = frontier.size();
			}
			Vertex v = frontier.delMin();
			if(v.cost < path_lengths[v.id]) {
				path_lengths[v.id] = v.cost;
				for(int adj : graph[v.id].keySet()) {
					if(path_lengths[adj] == 1000000) {
						frontier.insert(new Vertex(adj, graph[v.id].get(adj) + v.cost));
					}
				}
			}
		}
		StdOut.println(max);
	}
	
	public int get_path_len(int v) {
		return path_lengths[v];
	}
	
	public static void main(String[] args) throws IOException {
		int n = Integer.parseInt(args[1]);
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		HashMap<Integer, Integer>[] graph = (HashMap<Integer, Integer>[]) new HashMap[n];		
		for(int i = 0; i < n; i++) {
			graph[i] = new HashMap<Integer, Integer>();
			String[] line = in.readLine().split("\\s+");
			for(int j = 1; j < line.length; j++) {
				String[] adj = line[j].split(",");
				graph[i].put(Integer.parseInt(adj[0])-1, Integer.parseInt(adj[1]));
			}
		}
		in.close();
		Stopwatch sw = new Stopwatch();
		Dijkstra sp = new Dijkstra(graph, 0);
		StdOut.println("\nTiming: " + sw.elapsedTime() + "\n");
		int[] targets = new int[] {7,37,59,82,99,115,133,165,188,197};
		for(int i = 0; i < targets.length-1; i++) {
			StdOut.print(sp.get_path_len(targets[i] - 1) + ",");
		}
		StdOut.print(sp.get_path_len(targets[targets.length-1] - 1));
	}
}